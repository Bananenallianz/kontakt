#ifndef TRIE_HPP
#define TRIE_HPP

#include <string>
#include <map>
#include <vector>
#include <algorithm>

#include <cassert>

template<typename AnnotationT>
class Annotated {
    AnnotationT annotation;
};
template<>
class Annotated<void> { };

namespace {
template<typename CharT, typename Iter>
std::basic_string_view<CharT> make_view(const std::basic_string_view<CharT>& view, const Iter& begin, const Iter& end) {
    return view.substr(std::distance(view.begin(), begin), std::distance(view.begin(), end));
}
}

template<typename CharT, typename AnnotationT = void>
class trie_node : Annotated<AnnotationT> {
public:
    using string_t = std::basic_string<CharT>;
    using view_t = std::basic_string_view<CharT>;
    using char_t = CharT;
    size_t size;

    string_t content;
    bool is_leaf;
    std::map<CharT, trie_node> children;

    trie_node(const string_t& s = string_t(), bool is_leaf = false);

    trie_node(const trie_node&) = default;
    trie_node(trie_node&&) = default;
    trie_node& operator=(const trie_node&) = default;
    trie_node& operator=(trie_node&&) = default;

    bool insert(const view_t& s);
    bool erase(const view_t& s);
    bool lookup(const view_t& s) const;
    bool empty() const { return !is_leaf && std::all_of(children.begin(), children.end(), [](auto c) { return c.second.empty();});}

    void collect(const string_t& prefix, std::vector<string_t>& ) const;
};

template<typename CharT, typename AnnotationT = void>
class basic_trie {
public:
    trie_node<CharT, AnnotationT> root;
public:
    using string_t = std::basic_string<CharT>;
    using node_t = trie_node<CharT, AnnotationT>;

    basic_trie() = default;
    basic_trie(const basic_trie&) = default;
    basic_trie(basic_trie&&) = default;
    basic_trie& operator=(const basic_trie&) = default;
    basic_trie& operator=(basic_trie&&) = default;

    bool insert(const string_t& s) { return root.insert(s); }
    bool erase(const string_t& s) { return root.erase(s); };
    bool lookup(const string_t& s) const { return root.lookup(s); };

    size_t size() const { return root.size; }

    std::vector<string_t> collect() const;
};

using trie = basic_trie<char>;

// Implementation

template<typename CharT, typename AnnotationT>
trie_node<CharT, AnnotationT>::trie_node(const trie_node::string_t &s, bool is_leaf)
    : content(s), is_leaf(is_leaf), size(is_leaf ? 1 : 0) { }

template<typename CharT, typename AnnotationT>
bool trie_node<CharT, AnnotationT>::insert(const trie_node::view_t &str) {
    auto [content_pos, str_pos] = std::mismatch(content.begin(), content.end(), str.begin(), str.end());
    {};
    if (content_pos == content.end()) {
        if (str_pos == str.end()) {
            // already contained?
            if (!is_leaf) {
                ++size;
                is_leaf = true;
            }
            return false;
        } else if (children.empty() && !is_leaf) {
            // no other entries
            content = str;
            is_leaf = true;
            ++size;
            return true;
        } else {
            // recursive insertion
            char_t c = *str_pos;
            auto& child = children[c];
            bool inserted = child.insert(make_view(str, str_pos, str.end()));
            if (inserted) ++size;
            return inserted;
        }
    } else {
        assert(*content_pos != *str_pos);
        auto len_content = std::distance(content.begin(), content_pos);
        auto str_len = std::distance(str.begin(), str_pos);

        auto split_child = trie_node(string_t(content_pos, content.end()), is_leaf);
        std::swap(split_child.children, children);
        auto [child, _] = children.emplace(*content_pos, std::move(split_child));
        child->second.size = size;
        content = string_t(content.begin(), content_pos);
        if (str_pos != str.end()) {
            children.emplace(*str_pos, trie_node(string_t(str_pos, str.end()), true));
            is_leaf = false;
        } else {
            // str ends at split boundary
            is_leaf = true;
        }
        ++size;
        return true;
    }
}

template<typename CharT, typename AnnotationT>
bool trie_node<CharT, AnnotationT>::erase(const trie_node::view_t &s) {
    if (s.length() < content.length()) return false;
    auto [a, b] = std::mismatch(content.begin(), content.end(), s.begin(), s.end());
    bool erased = false;
    if (a == content.end()) {
        if (b == s.end()) {
            if (is_leaf) {
                is_leaf = false;
                --size;
                erased = true;
            }
        } else {
            auto child = children.find(*b);
            if (child != children.end()) {
                if (child->second.erase(make_view(s, b, s.end()))) {
                    --size;
                    if (child->second.empty())
                        children.erase(child);
                    erased = true;
                }
            }
        }
    }
    if (children.empty() && !is_leaf) content.clear();
    else if (children.size() == 1) {
        auto& node = children.begin()->second;
        content = content + node.content;
        size = node.size;
        is_leaf = node.is_leaf;
        std::map<CharT, trie_node> tmp_children = std::move(node.children);
        children = std::move(tmp_children);
    }
    return erased;
}

template<typename CharT, typename AnnotationT>
bool trie_node<CharT, AnnotationT>::lookup(const trie_node::view_t &s) const {
    if (s.length() < content.length()) return false;
    auto [a, b] = std::mismatch(content.begin(), content.end(), s.begin(), s.end());
    {};
    if (a == content.end()) {
        if (b == s.end()) return is_leaf;
        auto child = children.find(*b);
        if (child != children.end())
            return child->second.lookup(make_view(s, b, s.end()));
    }
    return false;
}

template<typename CharT, typename AnnotationT>
void trie_node<CharT, AnnotationT>::collect(const trie_node::string_t &prefix, std::vector<trie_node::string_t> & words) const {
    auto w = prefix + content;
    if (is_leaf) words.emplace_back(w);
    for (const auto& [k,v]: children) {
        v.collect(w, words);
    }
}



template<typename CharT, typename AnnotationT>
std::vector<typename basic_trie<CharT, AnnotationT>::string_t> basic_trie<CharT, AnnotationT>::collect() const {
    std::vector<string_t> contents;
    root.collect(string_t(), contents);
    return contents;
}

#endif // TRIE_HPP
