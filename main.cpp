#include <iostream>
#include <fstream>
#include <iomanip>
#include "trie.hpp"

using namespace std;

template<typename T>
std::ostream& operator<<(std::ostream& cout, const std::vector<T>& v) {
    bool sep = false;
    cout << '[';
    for (const T& e: v) {
        if (sep) cout << ", ";
        sep = true;
        cout << e;
    }
    cout << ']';
    return cout;
}

std::ostream& operator<<(std::ostream& cout, const trie_node<char>& t) {
    cout << '(';
    cout << '"' << t.content << "\", {";
    bool sep = false;
    for (const auto& [k,v]: t.children) {
        if (sep) cout << ", ";
        sep = true;
        cout << k << ':' << v;
    }
    cout << "})";
    return cout;
}

void collect(vector<pair<size_t, string>>& words, const trie_node<char>& t, string prefix = string(), size_t offset = 0) {
    string w = prefix + t.content;
    if (t.is_leaf) words.emplace_back(offset, w);
    for (auto [k,v]: t.children) {
        collect(words, v, w, offset + 1);
    }
}

int rate(const string_view& w, const trie_node<char>& t) {
    if (w.length() < t.content.length()) return -1;
    auto [a,b] = mismatch(t.content.begin(), t.content.end(), w.begin(), w.end());
    if (a == t.content.end()) {
        if (b == w.end()) {
            if (t.is_leaf) return 0;
            return -1;
        }
        auto child = t.children.find(*b);
        if (child != t.children.end()) {
            int r = rate(make_view(w, b, w.end()), child->second);
            if (r != -1) return r + 1;
        }
        return -1;
    }
    return -1;
}

int mainx(int argc, char *argv[])
{
    if (argc != 2) {
        cerr << "missing file name" << endl;
        return 1;
    }
    trie t;
    ifstream in(argv[1]);
    if (!in.is_open()) {
        cerr << "could not open file" << endl;
        exit(EXIT_FAILURE);
    }
    size_t linecount = 0;
    for (std::string line; std::getline(in, line); ++linecount) {
        std::transform(line.begin(), line.end(), line.begin(), ::tolower);
        t.insert(line);
    }
    cout << linecount << " Grundformen gelesen (" << t.size() << " verschiedene)" << endl;
    vector<pair<size_t, string>> words;
    collect(words, t.root);
    sort(words.begin(), words.end(), greater());
    //words.resize(200);
    words.erase(remove_if(words.begin(), words.end(), [](auto& x) {return x.first < 14;}), words.end());
    cout << words.size() << " Worte behalten" << endl;
    size_t len = max_element(words.begin(), words.end(), [](auto& lhs, auto& rhs) {return lhs.second.size() < rhs.second.size();})->second.size();
    for (auto [score, w]: words) {
        cout << setw(len + 1) << left << w << setw(3) << right << score << endl;
    }
    while (cin) {
        string s;
        cin >> s;
        cout << rate(s, t.root) << endl;
    }
    return 0;
}

void load(const string& name, trie& t) {
    ifstream in(name);
    if (!in.is_open()) {
        cerr << "could not open file" << endl;
        return;
    }
    size_t linecount = 0;
    for (std::string line; std::getline(in, line); ) {
        std::transform(line.begin(), line.end(), line.begin(), ::tolower);
        linecount += t.insert(line);
    }
    cout << "loaded " << linecount << " words" << endl;
}

void top(trie& t, size_t n) {
    vector<pair<size_t, string>> words;
    collect(words, t.root);
    sort(words.begin(), words.end(), greater());
    words.resize(min(n, words.size()));
    //words.erase(remove_if(words.begin(), words.end(), [](auto& x) {return x.first < 14;}), words.end());
    //cout << words.size() << " Worte behalten" << endl;
    if (words.empty()) return;
    size_t len = max_element(words.begin(), words.end(), [](auto& lhs, auto& rhs) {return lhs.second.size() < rhs.second.size();})->second.size();
    for (auto [score, w]: words) {
        cout << setw(3) << right << score << ' ' << setw(len + 1) << left << w << endl;
    }
}

vector<string> complete(const string_view& w, const trie_node<char>& t, const string& prefix = string()) {
    string p = prefix + t.content;
    vector<string> completions;
    auto [a,b] = mismatch(t.content.begin(), t.content.end(), w.begin(), w.end());
    if (a != t.content.end()) {
        if (b != w.end()) {
            return {};
        }
        if (t.is_leaf) completions.push_back(p);
        for (auto [k,v]: t.children)
            completions.push_back(p + v.content);
    } else if(b == w.end()) {
        if (t.is_leaf)
            completions.push_back(p);
        for (auto [k,v]: t.children)
            completions.push_back(p + v.content);
    } else {
        auto child = t.children.find(*b);
        if (child == t.children.end())
            return {};
        return complete(make_view(w, b, w.end()), child->second, p);
    }
    return completions;
}

int main() {
    trie t;
    while (cin) {
        char op;
        cin >> op;
        switch (op) {
        case 'a': {
            string s;
            cin >> s;
            t.insert(s);
            break;
        }
        case 'p':
            cout << t.size() << " " << t.collect() << endl;
            cout << t.root << endl;
            break;
        case 'l': {
            string s;
            cin >> s;
            cout << boolalpha << t.lookup(s) << endl;
            break;
        }
        case 'd': {
            string s;
            cin >> s;
            t.erase(s);
            break;
        }
        case 'f': {
            string file;
            cin >> file;
            load(file, t);
            break;
        }
        case 'c': {
            string s;
            cin >> s;
            cout << complete(s, t.root) << endl;
            break;
        }
        case 'r': {
            string w;
            cin >> w;
            cout << rate(w, t.root) << endl;
            break;
        }
        case 't': {
            int n;
            cin >> n;
            top(t, n);
            break;
        }
        case 'h': {
            cout << "a: add word\np: print trie\nl: lookup word\nd: delete word\nf: load wordlist\nr: rate word\nt: top words\nc: show completions\ne: clear trie" << endl;
            break;
        }
        case 'e':
            t = trie();
            break;
        default:
            cerr << "unknown operation" << endl;
        }
    }
    return 0;
}
